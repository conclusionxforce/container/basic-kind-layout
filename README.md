Getting Started
=============

Basic Kubeernetes cluster with kind. The following components are configured:
- Cluster with 1 master, 2 workers
- Ingress
- Container registry hosted on docker


A `Makefile` is provided to get started. Check the info command.

```bash
make info
```

- Create cluster
```bash
kind create cluster --config kind-config.yaml
```

- Setup Kubernetes Dashboard
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.3/aio/deploy/recommended.yaml
kubectl apply -f dashboard/service-account.yaml
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
kubectl proxy
firefox http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
```

- Setup Ingress NGINX
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
```

Test the ingress functionality
```bash
kubectl apply -f https://kind.sigs.k8s.io/examples/ingress/usage.yaml

# should output "foo"
curl localhost/foo

# should output "bar"
curl localhost/bar
```
