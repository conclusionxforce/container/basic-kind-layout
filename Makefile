.PHONY: all

info:
	@echo "Available commands:"
	@echo ""
	@echo "Command                              Description"
	@echo "-------                              -----------"
	@echo "make info                            Show the available make commands"
	@echo "make start                           Start the Kind environment"
	@echo "make delete                          Delete the Kind environment"
	@echo "make ingress-nginx                   Setup Ingress NGINX"
	@echo "make registry                        Setup registry"
	@echo ""

start: cluster-start ingress-nginx registry

cluster-start:
	@echo "=== Starting the Kind environment ==="
	kind create cluster --config kind-config.yaml

delete:
	@echo "=== Deleting the Kind environment ==="
	kind delete cluster
	docker stop kind-registry
	docker rm kind-registry

ingress-nginx:
	@echo "=== Deploy NGINX Ingress environment ==="
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

registry:
	@echo "=== Deploy Registry ==="
	# create registry container unless it already exists
	reg_name='kind-registry'
	reg_port='5000'
	running="docker inspect -f '{{.State.Running}}' "kind-registry" 2>/dev/null || true)"
	if [ "$$running" != 'true' ]; then \
		docker run -d --restart=always -p "5000:5000" --name "kind-registry" registry:2; \
	fi
	
	docker network connect "kind" "kind-registry"
	
	for node in `kind get nodes`; do \
		kubectl annotate node $$node "kind.x-k8s.io/registry=localhost:5000"; \
	done
